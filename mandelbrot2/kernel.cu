#include <cuda.h>
//#include <cuComplex.h>
#include "common_cu.hpp"


__global__ void mandelbrotKernel(char* output, int X, int Y,
                               size_t DIM_X, 
                               size_t DIM_Y) {
  
  //starting index of each block
  int idx = X + (blockIdx.x * DIM_X + threadIdx.x);
  int idy = Y + (blockIdx.y * DIM_Y + threadIdx.y);

  __shared__ int tl, tr, bl, br, mi;


   // If the dimensions are small enough, just do all of the work
  if(DIM_X <= blockDim.x || DIM_Y <= blockDim.y ){
	if(threadIdx.x < DIM_X && threadIdx.y < DIM_Y){
		const Complex c = getComplexFromIndex(idx,idy);               
        	output[idy * OUTPUT_SIZE_X + idx] =  iters(c, 255);
	}
  }else { // otherwise, see if we need to refine the mesh
	// Check four corners
	
	if(threadIdx.x == 0 && threadIdx.y == 0){
		tl = iters(getComplexFromIndex(idx,idy), 255);
	}if(threadIdx.x == blockDim.x - 1 && threadIdx.y == 0){
		tr = iters(getComplexFromIndex(idx - threadIdx.x + DIM_X-1, idy), 255);
	}if(threadIdx.x == 0 && threadIdx.y == blockDim.y - 1){
		bl = iters(getComplexFromIndex(idx, idy - threadIdx.y + DIM_Y-1), 255);
	}if(threadIdx.x == blockDim.x - 1 && threadIdx.y == blockDim.y - 1) {
		br = iters(getComplexFromIndex(idx - threadIdx.x + DIM_X-1, idy - threadIdx.y + DIM_Y-1), 255);
	}if(threadIdx.x == blockDim.x/2 && threadIdx.y == blockDim.y/2){
		mi = iters(getComplexFromIndex(idx - threadIdx.x + DIM_X/2, idy - threadIdx.y + DIM_Y/2), 255);
	}

	__syncthreads();

	// If there is substantial difference, refine:
	const int MAX_DIFF = 2;
    	if (abs(tl - mi) > MAX_DIFF  ||
       	    abs(tr - mi) > MAX_DIFF  ||
            abs(bl - mi) > MAX_DIFF  ||
       	    abs(br - mi) > MAX_DIFF) {
		if(threadIdx.x == 0 && threadIdx.y == 0){

		   // Sub-divide into 16 spaces
		   mandelbrotKernel<<< gridDim, blockDim >>> (output, idx, idy, DIM_X/gridDim.x, DIM_Y/gridDim.y);
		}

			
	}else { // otherwise, write the average value out everywhere
		const int avgIters = (tl + tr + bl + br) / 4;
		for (int k = idy; k < idy - threadIdx.y + DIM_Y; k+=blockDim.y){
			for(int j = idx; j < idx - threadIdx.x + DIM_X; j+=blockDim.x){
				output[k * OUTPUT_SIZE_X + j] = avgIters;
			}
		}
		
	}
	
	   
  }

}


