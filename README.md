CS 524 Final Project
====================

Carl Pearson (pearson@illinois)
Simon Garcia de Gonzalo (grcdgnz2@illinois)

Repository Layout
-----------------

Most directories are laid out with a few files: main.cu, kernel.cu, and support.cu
main.cu is the host code, kernel.cu is the device code, and support.cu is everything else.

### dynamiclaunchtime
This is the code used to measure the cost of dynamically launching a kernel.

### mandelbrot
C++ host-only mandelbrot with grid refinement.

### mandelbrot2
CUDA Mandelbrot with mesh refinement

### mandelbrot3
CUDA Mandelbrot witout mesh refinement.

### query
Device query code for making sure CUDA was installed properly. Not used in project.

### report
Contains the final report.

### rr
Token-passing CUDA code.

### stencil
Basic 3-point stencil CUDA code. Not used in project.

### stencil1
stencil-single from project.

### stencil2
stencil-multi from project.

### stencil3
not used in project.

### stencil3.2
stencil-central from project.

### stencil4
stencil-multi-sync from report

### vecadd
Vector add code for basic testing. Not used in project.
