typedef struct {
  bool valid;
  char data[20];
} Message;

__global__ void vecAddKernel(Message *mailboxes, int numActors) {

  const int tid = threadIdx.x + blockIdx.x * blockDim.x;
  const int dst = (tid + 1) % numActors;
  int done = 0;

  if (tid < numActors) {

    while (!done) {
      if (0 != mailboxes[tid].valid) {
        mailboxes[tid].valid = 0;
        mailboxes[dst] = mailboxes[tid];
        mailboxes[dst].valid = 1;
        done = 1;
      }
    }
  }
 
};
