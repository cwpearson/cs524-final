#include <cstdio>
#include <iostream>
#include "support.hpp"
#include "kernel.cu"

int main(int argc, char**argv) {

    Timer timer;
    cudaError_t cuda_ret;

    // Initialize host variables ----------------------------------------------

    printf("\nSetting up the problem..."); fflush(stdout);
    startTime(&timer);

    unsigned int numActors;
    if(argc == 1) {
        numActors = 16;
    } else if(argc == 2) {
        numActors = atoi(argv[1]);
    } else {
        printf("\n    Invalid input parameters!"
           "\n    Usage: ./rr                   # use 100 actors, 1000 passes"
           "\n    Usage: ./rr <actors> <passes> #"
           "\n");
        exit(0);
    }

    Message *mailboxes_h = new Message[numActors];
    for (int i = 0; i < numActors; ++i) {
      mailboxes_h[i].valid = 0;
    }
    mailboxes_h[0].valid = 1;

    stopTime(&timer); printf("%f s\n", elapsedTime(timer));
    printf("    actors = %u\n", numActors);

    // Allocate device variables ----------------------------------------------

    printf("Allocating device variables..."); fflush(stdout);
    startTime(&timer);

    Message* mailboxes_d;
    cuda_ret = cudaMalloc((void**) &mailboxes_d, sizeof(Message)*numActors);
	if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Copy host variables to device ------------------------------------------

    printf("Copying data from host to device..."); fflush(stdout);
    startTime(&timer);

    cuda_ret = cudaMemcpy(mailboxes_d, mailboxes_h, sizeof(Message) * numActors, cudaMemcpyHostToDevice);
	if(cuda_ret != cudaSuccess) FATAL("Unable to copy memory to device");

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Launch kernel ----------------------------------------------------------

    printf("Launching kernel..."); fflush(stdout);
    startTime(&timer);

    const unsigned int THREADS_PER_BLOCK = 512;
    const unsigned int numBlocks = (numActors - 1)/THREADS_PER_BLOCK + 1;
    dim3 gridDim(numBlocks, 1, 1), blockDim(THREADS_PER_BLOCK, 1, 1);
    vecAddKernel<<< gridDim, blockDim >>> (mailboxes_d, numActors);

    cuda_ret = cudaDeviceSynchronize();
	if(cuda_ret != cudaSuccess) FATAL("Unable to launch kernel");
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Copy device variables from host ----------------------------------------

    printf("Copying data from device to host..."); fflush(stdout);
    startTime(&timer);
    cuda_ret = cudaMemcpy(mailboxes_h, mailboxes_d,
                          sizeof(Message) * numActors, cudaMemcpyDeviceToHost);
    if(cuda_ret != cudaSuccess) FATAL("Unable to copy memory from device");


    //INSERT CODE HERE
    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Verify correctness -----------------------------------------------------

    //for (int i = 0; i < numActors; ++i) {
    //  std::cout << mailboxes_h[i].valid << " ";
    //}
    //std::cout << std::endl;

    // Free memory ------------------------------------------------------------

    free(mailboxes_h);
    cudaFree(mailboxes_d);

    return 0;

}
