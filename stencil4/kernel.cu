

__global__ void stencilKernel4(int *sync, float* inputs, float* outputs, 
                               const int INPUT_SIZE_X, 
                               const int INPUT_SIZE_Y,
                               int x, int y) {
  
  const int ACTOR_SIZE_X = blockDim.x;
  const int ACTOR_SIZE_Y = blockDim.y;
  const int ACTOR_LAUNCHES_X = (INPUT_SIZE_X - 1) / ACTOR_SIZE_X + 1;

  const int syncx = x / ACTOR_SIZE_X;
  const int syncy = y / ACTOR_SIZE_Y;


  if(threadIdx.x == 0 && threadIdx.y == 0) {
        
    if (x + ACTOR_SIZE_X < INPUT_SIZE_X - 1) {
      // Find the synchronization variable for this point
      const int syncid = syncy * ACTOR_LAUNCHES_X + syncx + 1;
      int old = atomicAdd(&sync[syncid], 1);
      if (x == 0 || y == 0 || old == 1) { // launch if on top or left or second attempt
        cudaStream_t s;
        cudaStreamCreateWithFlags(&s, cudaStreamNonBlocking);
        stencilKernel4<<< gridDim, blockDim, 0, s >>>(sync, inputs, outputs,
                                                      INPUT_SIZE_X, INPUT_SIZE_Y, x + ACTOR_SIZE_X, y);
      }
    }  

    if (y + ACTOR_SIZE_Y < INPUT_SIZE_Y - 1) {
      // Find the synchronization variable for this child
      const int syncid = (syncy+1) * ACTOR_LAUNCHES_X + syncx;
      int old = atomicAdd(&sync[syncid], 1); // add a launch attempt
      if (x == 0 || y == 0 || old == 1) { // launch if on top or left or second attempt
        cudaStream_t s;
        cudaStreamCreateWithFlags(&s, cudaStreamNonBlocking);
        stencilKernel4<<< gridDim, blockDim, 0, s >>>(sync, inputs, outputs,
                                                INPUT_SIZE_X, INPUT_SIZE_Y, x, y + ACTOR_SIZE_Y);
      }
    }
  }

  if((x + threadIdx.x) < INPUT_SIZE_X - 1 && (y + threadIdx.y) < INPUT_SIZE_Y - 1) {

    int i = (y + threadIdx.y) * INPUT_SIZE_X + (x + threadIdx.x);
    int n_right = (y + threadIdx.y)*INPUT_SIZE_X + ((x + threadIdx.x)+1);
    int n_down = ((y + threadIdx.y)+1)*INPUT_SIZE_X + (x + threadIdx.x);

    outputs[i] = inputs[i] + inputs[n_right] + inputs[n_down];
  }

}

