#include "support.hpp"
#include "kernel.cu"
#include <iostream>
#include <cstdio>

int main(int argc, char**argv) {

    Timer timer;
    cudaError_t cuda_ret;

    // Initialize host variables ----------------------------------------------

    printf("\nSetting up the problem..."); fflush(stdout);
    startTime(&timer);

    int DIM_X, DIM_Y;
    int ACTOR_SIZE_X, ACTOR_SIZE_Y;
    if(argc == 1) {
      DIM_X = 12;
      DIM_Y = 12;
      ACTOR_SIZE_X = 4;
      ACTOR_SIZE_Y = 4;
    } else if(argc == 5) {
        DIM_X = std::atoi(argv[1]);
        DIM_Y = std::atoi(argv[2]);
        ACTOR_SIZE_X = std::atoi(argv[3]);
        ACTOR_SIZE_Y = std::atoi(argv[4]);
    } else {
        printf("\n    Invalid input parameters!"
           "\n    Usage: ./vecadd             # 12 12 4 4 is used"
           "\n    Usage: ./vecadd <x> <y>     # Dimension of x,y is used"
           "\n");
        exit(0);
    }

    //const int ACTOR_LAUNCHES_X = (DIM_X-1) / ACTOR_SIZE_X + 1;
    //const int ACTOR_LAUNCHES_Y = (DIM_Y-1) / ACTOR_SIZE_Y + 1;

    // Input
    float *A_h = new float[DIM_Y * DIM_X];
    for (unsigned int i=0; i < DIM_Y * DIM_X; ++i) { A_h[i] = (rand()%100)/100.0;}

    // Output
    float* B_h = new float[DIM_Y * DIM_X];
    for (unsigned int i=0; i < DIM_Y * DIM_X; ++i) { B_h[i] = -1;}

    stopTime(&timer); printf("%f s\n", elapsedTime(timer));
    printf("    Size y=%u, x=%u\n", DIM_X, DIM_Y);

    // Allocate device variables ----------------------------------------------

    printf("Allocating device variables..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    float* A_d;
    cuda_ret = cudaMalloc((void**) &A_d, sizeof(float)*DIM_X*DIM_Y);
	if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    float* B_d;
    cuda_ret = cudaMalloc((void**) &B_d, sizeof(float)*DIM_X*DIM_Y);
	if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Copy host variables to device ------------------------------------------

    printf("Copying data from host to device..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    cuda_ret = cudaMemcpy(A_d, A_h, sizeof(float)*DIM_X*DIM_Y, cudaMemcpyHostToDevice);
	if(cuda_ret != cudaSuccess) FATAL("Unable to copy memory to device");

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Launch kernel ----------------------------------------------------------

    printf("Launching kernel..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    dim3 gridDim(1, 1, 1), blockDim(ACTOR_SIZE_X, ACTOR_SIZE_Y, 1);
    stencilKernel2<<< gridDim, blockDim >>> (A_d, B_d, DIM_X, DIM_Y, 0, 0);
   
     cuda_ret = cudaDeviceSynchronize();
	if(cuda_ret != cudaSuccess) FATAL("Unable to launch kernel");
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Copy device variables from host ----------------------------------------

    printf("Copying data from device to host..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    cuda_ret = cudaMemcpy(B_h, B_d, sizeof(float)*DIM_X*DIM_Y, cudaMemcpyDeviceToHost);
	if(cuda_ret != cudaSuccess) FATAL("Unable to copy memory from device");

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Verify correctness -----------------------------------------------------

    printf("Verifying results..."); fflush(stdout);

    verify(A_h, B_h, DIM_X, DIM_Y);

    // Free memory ------------------------------------------------------------
    free(A_h);
    free(B_h);

    //INSERT CODE HERE
    cudaFree(A_d);
    cudaFree(B_d);

    return 0;
}
