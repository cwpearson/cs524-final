
__global__ void stencilKernel2(float* inputs, float* outputs, int Dimx, int Dimy, int x, int y) {

  const int ACTOR_SIZE_X = blockDim.x;
  const int ACTOR_SIZE_Y = blockDim.y;
  //const int ACTOR_LAUNCHES_X = (Dimx - 1) / ACTOR_SIZE_X + 1;  


  if(threadIdx.x == 0 && threadIdx.y == 0) {
    if (x+ACTOR_SIZE_X < Dimx - 1) {
      if (y == 0) {
        cudaStream_t s;
        cudaStreamCreateWithFlags(&s, cudaStreamNonBlocking);
        stencilKernel2<<< gridDim, blockDim, 0, s >>> (inputs, outputs, Dimx, Dimy, x+ACTOR_SIZE_X, y);
      }
    }
    if (y+ACTOR_SIZE_Y < Dimy - 1) {
      cudaStream_t s;
      cudaStreamCreateWithFlags(&s, cudaStreamNonBlocking);
      stencilKernel2<<< gridDim, blockDim, 0, s >>> (inputs, outputs, Dimx, Dimy, x, y+ACTOR_SIZE_Y);
    }
  }

    //INSERT KERNEL CODE HERE
    if((x + threadIdx.x) < Dimx-1 && (y + threadIdx.y) < Dimy-1)
    {

        int i = (y + threadIdx.y)*Dimx + (x + threadIdx.x);
        int n_right = (y + threadIdx.y)*Dimx + ((x + threadIdx.x)+1);
        int n_down = ((y + threadIdx.y)+1)*Dimx + (x + threadIdx.x);

        outputs[i] = inputs[i] + inputs[n_right] + inputs[n_down];
    }

     //do nothing
}

