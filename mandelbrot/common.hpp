#ifndef COMMON_HPP
#define COMMON_HPP

#include <cstdlib>
#include <complex>

typedef std::complex<double> Complex;

// Output image size, resolution, and window
const size_t OUTPUT_SIZE_X = 65536;
const size_t OUTPUT_SIZE_Y = 65536;
const Complex TL(-2.25, -1.5); // Top left
const Complex BR(0.75, 1.5);   // Bottom right
const size_t ASCII_VIEW_Y = 50;
const size_t ASCII_VIEW_X = 80;

// Convert an index into the output array into a complex number
static Complex getComplexFromIndex(size_t x, size_t y) {
  Complex ret;
  ret.real() = double(x)/double(OUTPUT_SIZE_X) * (BR.real() - TL.real()) + TL.real();
  ret.imag() = double(y)/double(OUTPUT_SIZE_Y) * (BR.imag() - TL.imag()) + TL.imag();
  return ret;
}

// Calculate escape iters, capping at maxIters
static char iters(const Complex &c, const size_t maxIters) {
  Complex z(0.0, 0.0);
  for (size_t i = 0; i < maxIters; ++i) {
    if (norm(z) > 4.0) return i;
    z = z * z + c;
  }
 
  return maxIters;
}

#endif
