#include "common.hpp"

#include <iostream>

void mandel(char *output, size_t X, size_t Y, size_t DIM_X, size_t DIM_Y ) {

  // If the dimensions are small enough, just do all of the work
  if (DIM_X < 16 || DIM_Y < 16) {
    for (size_t y = Y; y < DIM_Y+Y; ++y) {
      for (size_t x = X; x < DIM_X+X; ++x) {
        const Complex c = getComplexFromIndex(x,y);
        output[y * OUTPUT_SIZE_X + x] = iters(c, 255);
      }
    }
  } else { // otherwise, see if we need to refine the mesh
    // Check four corners
    const int tl = iters(getComplexFromIndex(X,Y), 255);
    const int tr = iters(getComplexFromIndex(X+DIM_X-1,Y), 255);
    const int bl = iters(getComplexFromIndex(X,Y+DIM_Y-1), 255);
    const int br = iters(getComplexFromIndex(X+DIM_X-1,Y+DIM_Y-1), 255);
    const int mi = iters(getComplexFromIndex(X+DIM_X/2,Y+DIM_Y/2), 255);

    // If there is substantial difference, refine:
    const int MAX_DIFF = 2;
    if (abs(tl - mi) > MAX_DIFF ||
       abs(tr - mi) > MAX_DIFF  ||
       abs(bl - mi) > MAX_DIFF  ||
       abs(br - mi) > MAX_DIFF) {
      // Sub-divide into 16 spaces
      for (size_t y = Y; y < DIM_Y+Y; y += DIM_Y/16) {
        for (size_t x = X; x < DIM_X+X; x += DIM_X/16) {
          mandel(output, x, y, DIM_X/16, DIM_Y/16);
        }
      }
    } else { // otherwise, write the average value out everywhere
      const int avgIters = (tl + tr + bl + br) / 4;
      for (size_t y = Y; y < Y+DIM_Y; ++y) {
        for (size_t x = X; x < X+DIM_X; ++x) {
          output[y * OUTPUT_SIZE_X + x] = avgIters;
        }
      }
    }

  }
}

int main(void) {

  char *output_refine = new char[OUTPUT_SIZE_X * OUTPUT_SIZE_Y];

/*
  #pragma omp parallel for schedule(dynamic)
  for (size_t y = 0; y < OUTPUT_SIZE_Y; ++y) {
    if (y%1000 == 0) std::cerr << y << std::endl;
    for (size_t x = 0; x < OUTPUT_SIZE_X; ++x) {
      const Complex c = getComplexFromIndex(x,y);\
      output_basic[y * OUTPUT_SIZE_X + x] = iters(c, 255);
    }
  }
*/
  for (size_t y = 0; y < OUTPUT_SIZE_Y; y += OUTPUT_SIZE_Y/16) {
    for (size_t x = 0; x < OUTPUT_SIZE_X; x += OUTPUT_SIZE_X/16) {
      mandel(output_refine, x, y, OUTPUT_SIZE_X/16, OUTPUT_SIZE_Y/16);
    }
  } 

/*
  for (size_t y = 0; y < OUTPUT_SIZE_Y; y += OUTPUT_SIZE_Y/ASCII_VIEW_Y) {
    for (size_t x = 0; x < OUTPUT_SIZE_X; x += OUTPUT_SIZE_X/ASCII_VIEW_X) {
      if (output_refine[y * OUTPUT_SIZE_X + x] > 10) std::cout << "*";
      else                                           std::cout << " ";
    }
    std::cout << "\n";
  }
*/
  std::cout.write(output_refine, 
                  sizeof(char)*OUTPUT_SIZE_X * OUTPUT_SIZE_Y);

}
