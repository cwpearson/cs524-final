#include "support.hpp"
#include "kernel.cu"
#include <iostream>
#include <cstdio>

int main(int argc, char**argv) {

    Timer timer;
    cudaError_t cuda_ret;

    // Initialize host variables ----------------------------------------------

    printf("\nSetting up the problem..."); fflush(stdout);
    startTime(&timer);

    int DIM_X, DIM_Y;
    if(argc == 1) {
      DIM_X = 12;
      DIM_Y = 12;  
    } else if(argc == 3) {
        DIM_X = std::atoi(argv[1]);
        DIM_Y = std::atoi(argv[2]);
    } else {
        printf("\n    Invalid input parameters!"
           "\n    Usage: ./vecadd             # Dimension of size 1000 is used"
           "\n    Usage: ./vecadd <x> <y>     # Dimension of x,y is used"
           "\n");
        exit(0);
    }

    // Input
    float *A_h = new float[DIM_Y * DIM_X];
    for (unsigned int i=0; i < DIM_Y * DIM_X; ++i) { A_h[i] = 1;} //(rand()%100)/100.00; }

    //scratch
    float *S_h = new float[DIM_Y * DIM_X];
    for (unsigned int i=0; i < DIM_Y * DIM_X; ++i) { A_h[i] = 1;}

    // Output
    float* B_h = new float[DIM_Y * DIM_X];
    for (unsigned int i=0; i < DIM_Y * DIM_X; ++i) { B_h[i] = -1;}

    stopTime(&timer); printf("%f s\n", elapsedTime(timer));
    printf("    Size y=%u, x=%u\n", DIM_X, DIM_Y);

    // Allocate device variables ----------------------------------------------

    printf("Allocating device variables..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    float* A_d;
    cuda_ret = cudaMalloc((void**) &A_d, sizeof(float)*DIM_X*DIM_Y);
	if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    float* S_d;
    cuda_ret = cudaMalloc((void**) &S_d, sizeof(float)*DIM_X*DIM_Y);
        if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    float* B_d;
    cuda_ret = cudaMalloc((void**) &B_d, sizeof(float)*DIM_X*DIM_Y);
	if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Copy host variables to device ------------------------------------------

    printf("Copying data from host to device..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    cuda_ret = cudaMemcpy(A_d, A_h, sizeof(float)*DIM_X*DIM_Y, cudaMemcpyHostToDevice);
	if(cuda_ret != cudaSuccess) FATAL("Unable to copy memory to device");

    cuda_ret = cudaMemcpy(S_d, S_h, sizeof(float)*DIM_X*DIM_Y, cudaMemcpyHostToDevice);
        if(cuda_ret != cudaSuccess) FATAL("Unable to copy memory to device");

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Launch kernel ----------------------------------------------------------

    printf("Launching kernel..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    const unsigned int THREADS_PER_BLOCK = 16; //1024;
    const unsigned int numBlocks_x = (DIM_X - 1)/THREADS_PER_BLOCK + 1;
    const unsigned int numBlocks_y = (DIM_Y - 1)/THREADS_PER_BLOCK + 1;
    dim3 gridDim(numBlocks_x, numBlocks_y, 1), blockDim(4, 4, 1);

    int num_layers = 1; 

    stencilKernel3<<< gridDim, blockDim >>> (A_d, B_d, S_d, DIM_X, DIM_Y, num_layers, 1, 1);
    
    cuda_ret = cudaDeviceSynchronize();
	if(cuda_ret != cudaSuccess) FATAL("Unable to launch kernel");
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Copy device variables from host ----------------------------------------

    printf("Copying data from device to host..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    cuda_ret = cudaMemcpy(B_h, B_d, sizeof(float)*DIM_X*DIM_Y, cudaMemcpyDeviceToHost);
	if(cuda_ret != cudaSuccess) FATAL("Unable to copy memory from device");

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Verify correctness -----------------------------------------------------

    printf("Verifying results..."); fflush(stdout);

    verify(A_h, B_h, DIM_X, DIM_Y);

    // Free memory ------------------------------------------------------------
    free(A_h);
    free(B_h);

    //INSERT CODE HERE
    cudaFree(A_d);
    cudaFree(B_d);

    return 0;
}
