
__global__ void stencilKernel3(float* inputs, float* outputs, float* scratch, int Dimx, int Dimy, int layers, int bid_x, int bid_y) {

    // Calculate global thread index based on the block and thread indices ----
    int idx = (blockIdx.x*bid_x) * blockDim.x + threadIdx.x;
    int idy = (blockIdx.y*bid_y) * blockDim.y + threadIdx.y;

    int i = idy*Dimx + idx;


     //INSERT KERNEL CODE HERE
    if(idx < Dimx-1 && idy < Dimy-1)
    {

        int n_right = (idy*Dimx + (idx +1));
        int n_down = ((idy+1)*Dimx + idx);

        scratch[i] = inputs[i] + inputs[n_right] + inputs[n_down];
	//outputs[i] = inputs[i] + inputs[n_right] + inputs[n_down];

	if(threadIdx.x == blockDim.x-1 && idx+1 < Dimx-1) //coudl be a problem
	{
		i = idy*Dimx + (idx+1);
        	n_right = idy*Dimx + (idx+2);
        	n_down = (idy+1)*Dimx + (idx+1);
		
		scratch[i] = inputs[i] + inputs[n_right] + inputs[n_down]; //could be a segfault here
	}
	
	if(threadIdx.y == blockDim.y-1 && idy+1 < Dimy-1)
	{
		i = (idy+1)*Dimx + idx;
                n_right = (idy+1)*Dimx + (idx+1);
                n_down = (idy+2)*Dimx + idx;

                scratch[i] = inputs[i] + inputs[n_right] + inputs[n_down]; //could be a segfault here
	}

    }
    
    __syncthreads();    

    if(threadIdx.x == 0 && threadIdx.y == 0 && layers > 0)
    {
        dim3 gridDim(1, 1, 1);
	dim3 blockDim(32,32, 1);
        
        stencilKernel3<<< gridDim, blockDim, 0 >>> (scratch, outputs, inputs, Dimx, Dimy, layers-1, blockIdx.x*bid_x, blockIdx.y*bid_y);

    }

    __syncthreads();

    if(idx < Dimx-1 && idy < Dimy-1)
    {
	
	outputs[i] = scratch[i];

    }


     //do nothing
}

