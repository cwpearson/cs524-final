#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "support.hpp"

static int index2d(int y, int x, int DIM_X) {
  return y * DIM_X + x;
}

void verify(float *A, float *B, int DIM_X, int DIM_Y) {

  const float relativeTolerance = 1e-6;

  for(int y = 0; y < DIM_Y - 1; ++y) {
    printf("\n");
    for (int x = 0; x < DIM_X - 1; ++x) {
      float sum = A[index2d(y+1,x,DIM_X)] + 
                  A[index2d(y,x+1,DIM_X)] + 
                  A[index2d(y,x,DIM_X)];
      printf("%f ", B[index2d(y,x,DIM_X)]);
      float relativeError = (sum - B[index2d(y,x,DIM_X)])/sum;
      /*if (relativeError > relativeTolerance
        || relativeError < -relativeTolerance) {
        printf("TEST FAILED\n\n");
        exit(0);
      }*/
    }
  }
  printf("TEST PASSED\n\n");
}

void startTime(Timer* timer) {
    gettimeofday(&(timer->startTime), NULL);
}

void stopTime(Timer* timer) {
    gettimeofday(&(timer->endTime), NULL);
}

float elapsedTime(Timer timer) {
    return ((float) ((timer.endTime.tv_sec - timer.startTime.tv_sec) \
                + (timer.endTime.tv_usec - timer.startTime.tv_usec)/1.0e6));
}
