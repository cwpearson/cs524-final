/******************************************************************************
 *cr
 *cr         (C) Copyright 2010-2013 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ******************************************************************************/

// ============================================================================
// NOTE TO INSTRUCTORS: SOLUTION CODES ARE FOR INSTRUCTOR REFERENCE ONLY.
//    PLEASE DO NOT SHARE WITH STUDENTS
// ============================================================================

__global__ void stencilKernel1(float* inputs, float* outputs, int Dimx, int Dimy, int x, int y) {

    // Calculate global thread index based on the block and thread indices ----

    if (x+1 < Dimx - 1)
      if ( y == 0 ) {
        cudaStream_t s;
        cudaStreamCreateWithFlags(&s, cudaStreamNonBlocking);
        stencilKernel1<<< 1, 1, 0, s>>> (inputs, outputs, Dimx, Dimy, x+1, y);
    }
 
    if (y+1 < Dimy - 1) {
      cudaStream_t s;
      cudaStreamCreateWithFlags(&s, cudaStreamNonBlocking);
      stencilKernel1<<< 1, 1, 0, s >>> (inputs, outputs, Dimx, Dimy, x, y+1);
    }

    //INSERT KERNEL CODE HERE
    if(x < Dimx-1 && y < Dimy-1)
    {

    	int i = y*Dimx + x;
    	int n_right = y*Dimx + (x+1);
    	int n_down = (y+1)*Dimx + x;  
    
    	outputs[i] = inputs[i] + inputs[n_right] + inputs[n_down];
     }

     //do nothing
}
