__global__ void stencilKernel(float* inputs, float* outputs,
                              int INPUT_SIZE_X, int INPUT_SIZE_Y) {

  const int x = threadIdx.x + blockIdx.x * blockDim.x;
  const int y = threadIdx.y + blockIdx.y * blockDim.y;

  if (x < INPUT_SIZE_X - 1 && y < INPUT_SIZE_Y - 1) {
    int i       = y       * INPUT_SIZE_X + x;
    int n_right = y       * INPUT_SIZE_X + x + 1;
    int n_down  = (y + 1) * INPUT_SIZE_X + x;  
    
    outputs[i] = inputs[i] + inputs[n_right] + inputs[n_down];
  }

}
