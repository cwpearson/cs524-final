#include "support.hpp"
#include "common_cu.hpp"
#include "kernel.cu"
#include <iostream>
#include <cstdio>

int main(int argc, char**argv) {

    Timer timer;
    cudaError_t cuda_ret;

    // Initialize host variables ----------------------------------------------

    printf("\nSetting up the problem..."); fflush(stdout);
    startTime(&timer);

    //int INPUT_SIZE_X, INPUT_SIZE_Y;
    int NUM_BLOCKS_X, NUM_BLOCKS_Y;
    int THREADS_PER_BLOCK_X, THREADS_PER_BLOCK_Y; 
    if(argc == 1) {
      //NUM_BLOCKS_X = 16;
      //NUM_BLOCKS_Y = 16;
      THREADS_PER_BLOCK_X = 1024;
      THREADS_PER_BLOCK_Y = 1024; 
    } else if(argc == 3) {
    	THREADS_PER_BLOCK_X = std::atoi(argv[1]);
	THREADS_PER_BLOCK_Y = std::atoi(argv[2]);
    } else {
        printf("\n    Invalid input parameters!"
           "\n    Usage: ./mandelbrot # 16 16 is used"
           "\n    Usage: ./vecadd <input x> <input y> <actor x> <actor y>"
           "\n");
        exit(0);
    }

    //const int ACTOR_LAUNCHES_X = (INPUT_SIZE_X-1) / THREADS_PER_BLOCK_x + 1;
    //const int ACTOR_LAUNCHES_Y = (INPUT_SIZE_Y-1) / THREADS_PER_BLOCK_y + 1;
    //std::cout << "xlaunches: " << ACTOR_LAUNCHES_X << "\n";
    //std::cout << "ylaunches: " << ACTOR_LAUNCHES_Y << "\n";




    // Input
    //float *A_h = new float[INPUT_SIZE_Y * INPUT_SIZE_X];
    //for (unsigned int i = 0; i < INPUT_SIZE_Y * INPUT_SIZE_X; ++i) { A_h[i] = 1;} //(rand()%100)/100.00; }

    // Output
    char* B_h = new char[OUTPUT_SIZE_Y * OUTPUT_SIZE_X];
    //for (unsigned int i = 0; i < INPUT_SIZE_Y * INPUT_SIZE_X; ++i) { B_h[i] = 1;}


    stopTime(&timer); printf("%f s\n", elapsedTime(timer));
    printf("    Size y=%u, x=%u\n", OUTPUT_SIZE_X, OUTPUT_SIZE_Y);

    // Allocate device variables ----------------------------------------------

    printf("Allocating device variables..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    //float* A_d;
    //cuda_ret = cudaMalloc((void**) &A_d, sizeof(float)*INPUT_SIZE_X*INPUT_SIZE_Y);
	//if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    char* B_d;
    cuda_ret = cudaMalloc((void**) &B_d, sizeof(char)*OUTPUT_SIZE_X*OUTPUT_SIZE_Y);
	if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    //float* temp;
    //cuda_ret = cudaMalloc((void**) &temp, sizeof(float));
     //   if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    //IcudaStream_t* s;
    //cuda_ret = cudaMalloc((void**) &s, sizeof(cudaStream_t)*THREADS_PER_BLOCK_x*THREADS_PER_BLOCK_x);
    //    if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Copy host variables to device ------------------------------------------

    printf("Copying data from host to device..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    /*cuda_ret = cudaMemcpy(A_d, A_h, sizeof(float)*INPUT_SIZE_X*INPUT_SIZE_Y,
                          cudaMemcpyHostToDevice);
    if(cuda_ret != cudaSuccess) FATAL("Unable to copy memory to device");


    cuda_ret = cudaMemcpy(B_d, B_h, sizeof(float)*INPUT_SIZE_X*INPUT_SIZE_Y,
                          cudaMemcpyHostToDevice);
    if(cuda_ret != cudaSuccess) FATAL("Unable to copy memory to device");
*/

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Launch kernel ----------------------------------------------------------

    printf("Launching kernel..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    dim3 gridDim((OUTPUT_SIZE_X - 1)/THREADS_PER_BLOCK_X + 1, (OUTPUT_SIZE_Y - 1)/THREADS_PER_BLOCK_Y + 1, 1);
    dim3 blockDim(THREADS_PER_BLOCK_X, THREADS_PER_BLOCK_Y, 1);
    mandelbrotKernel<<< gridDim, blockDim >>> (B_d);
   
     cuda_ret = cudaDeviceSynchronize();
	if(cuda_ret != cudaSuccess) FATAL("Unable to launch kernel");
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Copy device variables from host ----------------------------------------

    printf("Copying data from device to host..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    cuda_ret = cudaMemcpy(B_h, B_d, sizeof(char)*OUTPUT_SIZE_X*OUTPUT_SIZE_Y, cudaMemcpyDeviceToHost);
	if(cuda_ret != cudaSuccess) FATAL("Unable to copy memory from device");

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Verify correctness -----------------------------------------------------

    printf("Verifying results..."); fflush(stdout);
    
    for (size_t y = 0; y < OUTPUT_SIZE_Y; y += OUTPUT_SIZE_Y/ASCII_VIEW_Y) {
    for (size_t x = 0; x < OUTPUT_SIZE_X; x += OUTPUT_SIZE_X/ASCII_VIEW_X) {
      if (B_h[y * OUTPUT_SIZE_X + x] > 10) std::cout << "*";
      else                                           std::cout << " ";
    }
    std::cout << "\n";
    }

    //std::cout.write(B_h,
    //              sizeof(char)*OUTPUT_SIZE_X * OUTPUT_SIZE_Y);

    //verify(A_h, B_h, INPUT_SIZE_X, INPUT_SIZE_Y);

    // Free memory ------------------------------------------------------------
    //free(A_h);
    free(B_h);

    //INSERT CODE HERE
    //cudaFree(A_d);
    cudaFree(B_d);

    return 0;
}
