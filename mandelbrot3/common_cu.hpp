#ifndef COMMONCU_HPP
#define COMMONCU_HPP

#include <cstdlib>


class Complex {
 public:
  __device__ Complex(double a, double b) : re_(a), im_(b) {}
  __device__ Complex(){}

  __device__ double &real() {return re_;}
  __device__ double &imag() {return im_;}
  __device__ const double &real() const {return re_;}
  __device__ const double &imag() const {return im_;}

  __device__ Complex operator*(const Complex &rhs) {
    return Complex(real() * rhs.real() - imag() * rhs.imag(),
                   imag() * rhs.real() + real() * rhs.imag());
  }

  __device__ Complex operator+(const Complex &rhs) {
    return Complex(real() + rhs.real(), imag() + rhs.imag());
  }

 private:
  double re_;
  double im_;


};

__device__ static double norm(const Complex &c) {
  return c.real() * c.real() + c.imag() * c.imag();
}

// Output image size, resolution, and window
const size_t OUTPUT_SIZE_X = 65536;
const size_t OUTPUT_SIZE_Y = 65536;
//const Complex TL(-2.25, -1.5); // Top left
//const Complex BR(0.75, 1.5);   // Bottom right
const size_t ASCII_VIEW_Y = 50;
const size_t ASCII_VIEW_X = 80;

// Convert an index into the output array into a complex number
__device__ static Complex getComplexFromIndex(size_t x, size_t y) {
  const Complex TL(-2.25, -1.5); // Top left
  const Complex BR(0.75, 1.5);   // Bottom right

  Complex ret;
  ret.real() = double(x)/double(OUTPUT_SIZE_X) * (BR.real()-TL.real()) + TL.real();
  ret.imag() = double(y)/double(OUTPUT_SIZE_Y) * (BR.imag()-TL.imag()) + TL.imag();
  return ret;
}

// Calculate escape iters, capping at maxIters
__device__ static char iters(const Complex &c, const size_t maxIters) {
  Complex z(0.0, 0.0);
  for (size_t i = 0; i < maxIters; ++i) {
     if (norm(z) > 4.0) return i;
     z = z * z + c;
  }

   return maxIters;
}

#endif
