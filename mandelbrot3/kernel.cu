#include <cuda.h>
//#include <cuComplex.h>
#include "common_cu.hpp"


__global__ void mandelbrotKernel(char* output) {
  
  //starting index of each block
  int idx = (blockIdx.x * blockIdx.x + threadIdx.x);
  int idy = (blockIdx.y * blockIdx.y + threadIdx.y);

  if(idx < OUTPUT_SIZE_X  && idy < OUTPUT_SIZE_Y)
  {
	const Complex c = getComplexFromIndex(idx,idy);
        output[idy * OUTPUT_SIZE_X + idx] =  iters(c, 255);	
  }

}


