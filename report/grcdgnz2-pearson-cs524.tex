\documentclass[11pt]{article}
\usepackage[margin=1in]{geometry}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{listings}

\begin{document}

\setlength{\baselineskip}{14pt}

\title{Implications of CUDA and Dynmaic Parallelism
       as Foundations for an Actor Language \\
       University of Illinois CS 524 Final Project}
\author{Simon Garcia de Gonzalo \& Carl Pearson \\ 
        \texttt{\{grcdgnz2, pearson\} @ illinois.edu}}
\date{Dec 12 2014}

\maketitle

\section{Introduction}

We investigate the sutiability of Nvidia CUDA and CUDA's dynamic
parallelism as a foundation for an actor model implementation. The actor model
is desirable for its concurrency and safety semantics, and modern GPUs are
massively-parallel, highly-programmable, and widely-available. CUDA dynamic
parallelism provides the opportunity for dynamic on-GPU actor creation, potentially
allowing a full on-GPU actor system. Our experience in this project shows that 
CUDA's thread-block scheduler and CUDA's kernel lifetime semantics prove to be severe constraints on the present use of CUDA as a foundation for an actor
language
that requires any kind of synchronization or message passing.

\subsection{Project Materials}

All project code and a brief description is available here: \texttt{https://bitbucket.org/cwpearson/cs524-final/}

\subsection{Project Scope}

In this project, we attempt to correlate actor model features with aspects
of CUDA, and enumerate and evaluate any performance or programmability
constraints that come out of that correlation. Below are some things that could be
future work, but are not in the scope of this project.
\begin{itemize}
\item{We do not attempt to implement an actor language for GPUs.}
\item{We do not attempt to compare the performance of actor-style CUDA to any existing actor language or implementation.}
\end{itemize}

\section{CUDA Overview}

\subsection{CUDA Programming Model}

CUDA\cite{nvidia2008programming} is a throughput-oriented parallel programming
model and platform that targets Nvidia GPUs. Although traditional GPUs were
designed for pure graphics workloads, the last decade has seen the introduction
of an increasing amount of programmability into these devices. CUDA is a
system to harness that programmability and use GPUs for general-purpose
processing. GPUs architecture emphasizes parallel and relatively
slow execution of many threads instead of CPU-like rapid execution of few threads. To support this execution model, GPUs have peak FLOP and memory
bandwidth rates that exceed CPU rates by an order of magnitude.

CUDA extends C/C++ by allowing the programmer to define special functions
called \textit{kernels}. When called, these kernels are executed N times in
parallel by N threads on the GPU. All threads have the same behavior in
SIMT fashion.
Each thread belongs to a two-level hierarchy of \textit{threads} and \textit{blocks}.

\begin{figure}
\end{figure}

\begin{figure}[!ht]
  \centering
    \includegraphics[width=0.5\textwidth]{images/grid-of-thread-blocks.png}
  \caption
  {
    A two-dimensional CUDA grid of two-dimensional thread blocks. Each block
    is assigned an ID along the x- and y-dimension, as is each thread within
    each block. In this manner, work may be partitioned among threads and
    blocks based on their IDs.
    \label{fig:hierarchy}
  }
\end{figure}

At each level of the heirarchy, a thread is assigned one ID per dimension at
each level of the hierarchy. For example in Figure \ref{fig:hierarchy}, the
top-left thread in every block is given \texttt{threadIdx.x = 0} and
\texttt{threadIdx.y = 0} by the CUDA system. Every thread in the shown block
has \texttt{blockIdx.x = 1} and \texttt{blockIdx.y = 1}. This allows for simple
decomposition of many regular data structures, since the work may be easily
divided according to a thread's ID at various levels of the hierarchy.

All threads in a block execute the same instructions at the same time.
For example, both conditions of branches are executed, with opposing sets of threads masked
off each time. This means that if threads in a block take different paths through
conditional statements, performance severely suffers.

To bring it all together, consider the following vector addition kernel code
for computing \texttt{c = a + b} on arrays of size \texttt{n}. This code is
launched with a a one-dimensional grid of one-dimensional blocks, meaning
\texttt{blockIdx.y = 0} and \texttt{threadIdx.y = 0}.

\begin{minipage}{\linewidth}
\begin{lstlisting}

__global__ void vecAdd(float *c, float *a, float *b, int n){
   int id = blockIdx.x * blockDim.x + threadIdx.x;
   if (id < n)
     c[id] = a[id] + b[id];
}

\end{lstlisting}
\end{minipage}

The first line of the function computes a global ID. This is similar to the
pattern of
code for flattening an array index since each thread is embedded in a
multi-dimensional block structure.

Instead of looping over the values in the array, each thread computes only
a single element. That element is based on the global ID of the thread.
The computation is only done if the global ID is less than the size of the
array. This guard must be placed because all blocks launched are the same size.
For example, to add arrays of 384 elements we may launch two blocks of 256
threads, so all of the threads with \texttt{blockIdx.x = 0} and 1/2 of the
threads with \texttt{blockIdx.x = 1} should do any work.

Data is provided to the kernel through special \texttt{memcpy} functions since
the host and device do not share the same memory space. Therefore, the general
pattern of an entire CUDA program is:

\begin{enumerate}
  \item{Copy input data from host to GPU}
  \item{Determine kernel's grid and block dimensions from problem size}
  \item{Launch kernel (call kernel function)}
  \item{Copy output data from GPU to host}
\end{enumerate}

CUDA follows a co-processor model. The GPU is not allowed to call functions
on the CPU, nor interact with memory accessible to the CPU. Furthermore,
until recently CUDA did not allow GPUs to recursively call kernel functions.

\subsection{CUDA Dynamic Parallelism}

In November 2012, Nvidia released CUDA 5.0 with support for
\textit{dynamic parallelism}, the ability for GPU threads to launch 
GPU kernels. Dynamic parallelism enables a programming patterns that required
host code in
earlier version of CUDA.
CUDA code can now recurse on kernel invocations as well as function calls. This
enables data-dependant adjustment of parallelism. In many grid problems
the mesh that the solution is created over does not need the same amount of
detail in all locations. This allows the GPU to launch kernels with more
threads where there is more work to do at runtime, which makes better use
of available computational resources.

For the purposes of this project, dynamic parallelism's downsides are kernel launch overhead, and that a thread-block's lifetime must subsume that of its
children.

\begin{figure}[!ht]
  \centering
    \includegraphics[width=0.7\textwidth]{images/nesting.png}
  \caption
  {
    Child kernels launched cause parent threads to remain alive. Source:
    \cite{nvidia2012dynamic}
    \label{fig:nesting}
  }
\end{figure}

Figure \ref{fig:nesting} shows a CPU thread launching a kernel. One thread
within that kernel launches another kernel, and that thread remains active
until the child kernel completes.

A hardware limit of 24 levels of kernel nesting is imposed by current Nvidida GPUs.
The kernel launched by the host onto the GPU is treated no differently than GPU-to-GPU
kernel launches in this respect, so the first kernel on the GPU is at depth 1.

\section{CUDA and the Actor Model}

For CUDA to be a viable foundation for an actor language, it needs to
efficiently support crucial elements of the actor model. Those elements are

\begin{enumerate}
\item{Actor Creation - actors need to be able to create new actors.}
\item{Messaging - actors can pass messages to each other}
\item{Fairness - all messages and actions are eventually delivered and executed.}
\item{Concurrency - computations can occur simultaneously.}
\item{Isolation - actors cannot modify each others' state.}
\end{enumerate}

The following subsections describe how we map these elements of the actor model
onto CUDA programming constructs.

\subsection{Actor Creation}
We choose to model actors as CUDA thread-blocks.  This enables a single actor to
take advantage of GPU parallelism by having multiple threads within the
thread-block.

All thread-blocks and threads within a CUDA kernel share the same code. Since our
benchmarks are all scientific programs, this is not a problem for us.
A simple solution in general is to encode all desired behaviors into each
thread and select the desired behavior based on the thread or thread-block 
indices. 

Under this model, the only way to
create new actors is to launch new kernels. Dynamic parallelism is the feature
that enables us to do this on the GPU to avoid the overhead of communicating
with the host. This comes with two severe
restrictions: kernel launches can only be nested 24 deep, and parent kernels
remain active while child kernels exist. This means, for example, that an
actor cannot be destroyed while an actor that it created still exists.
Furthermore, it is impossible to have a long chain of actors creating new actors.

\subsection{Messaging}

In general, the only way GPU threads can communicate is through the GPU's
global memory
space. For some of our benchmarks we allocated part of this global memory space to
provide
each actor with a synchronization variable so it would know when the data it needed was ready.
Since our codes were scientific in nature, general message-passing was not needed, but a 
system such as tuplespaces in the global memory mixed with atomic operations on that tuplespace
could allow threads to send arbitrary messages to each other.

\subsection{Fairnesss}

Fairness is not supported by the current CUDA schedulers. Thread-blocks are assigned to
execution resources in a round-robin manner, and remain resident on those resources until the
block finishes executing.

For example, we developed on a Nvidia GPU with 16 streaming multiprocessors.
Each multiprocessor essentially supports 16-way SMT. If more than 257 thread-blocks are
created, one thread block will not run until another completes. This means that if
thread-blocks are actors, it is impossible to have more than 256 persistent actors
in the whole system. For our benchmarks, many actors are created to do some work
and then terminate, so this restriction was not a problem.

Where messaging was needed in our benchmarks it is implemented as
atomic operations on global memory, so lack of fairness in computation is the
same as lack of fairness in messaging.

\subsection{Concurrency and Isolation}

Concurrency is provided by the parallelism available in Nividia GPUs. For isolation there is
no mechanism in CUDA where threads may modify any local data belonging to another thread.
In these two respects, CUDA's programming model is perfectly aligned with the actor model.

\section{Results and Discussion}

We implemented four versions of a simple stencil code and two versions of a
mandelbrot fractal renderer to investigate some performance implications of using
CUDA in an actor-method style.

\subsection{Token-Passing}

We created a token-passing CUDA configuration to get an idea of how quickly CUDA could be used
to pass data between actors. In this code, actors are configured in a one-dimensional grid  and each actor is given a mailbox in global memory. The behavior is to spin on that mailbox
until it is full, then load the message and store it into the neighbor's mailbox. The actor
with the lowest ID has a full mailbox provided by the host code.

Each CUDA thread models an actor. This is different than following bechmarks where actors are
modeled by a thread block and threads within the block are used to take advantage of the GPU
parallelism.

\begin{figure}[!ht]
  \centering
  \begin{subfigure}{.48\textwidth}
    \centering
    \includegraphics[width=1\linewidth]{images/rr64.png}
    \caption{Passing 64B token}
  \end{subfigure}%
  \begin{subfigure}{.48\textwidth}
    \centering
    \includegraphics[width=1\linewidth]{images/rr1024.png}
    \caption{Passing 1KB token}
  \end{subfigure}%
  \caption{The left graph displays elapsed time for a 1D grid of actors passing a 64B token.
  The right graph displays elapsed time for a 1D grid of actors passing a 1KB token. In both
  cases each actor passes the token exactly once.}
  \label{fig:token}
\end{figure}

Figure \ref{fig:token} shows the performance results. The elapsed time scales linearly with the number of tokens, with each 64B token contributing
6.8 $\mu$s and each 1KB token contributing 319.6 $\mu$s. Small tokens are faster per byte
transferred, probably due to neighboring mailboxes sharing a cache line.

This token-passing arrangement is not guaranteed to progress for any number of actors. If
the number of actors causes more thread-blocks to be created than execution resources are
available, the first thread-block is not guaranteed to be scheduled and the whole system will
deadlock waiting for a token that will never arrive.

\subsection{Sequential vs Parallel Actors}

We created two different stencil codes to investigate the performance and scalability
limitiations of mapping actors onto threads. Each of these codes do a simple three-point
stencil computation over an array of C++ \texttt{float}s.

As soon as an actor is finished it creates one or two actors adjacent to it.
Figure \ref{fig:stencil12pattern} shows the actor creation pattern that is used to
cover the
whole image space. Whether one or two neighboring actors are created depends on
the location
of the actor in the output image.

In \texttt{stencil-single}, the actors are CUDA threadblocks with a single thread, which computes a single element of the output. \texttt{stencil-multi} aguments this system by
creating multiple threads in each thread block for higher performance and a larger
supported input and output size.

\begin{figure}[!ht]
  \centering
  \begin{subfigure}{.45\textwidth}
    \centering
    \includegraphics[width=.7\linewidth]{images/stencil1_actors.png}
    \caption{\texttt{stencil-single}}
  \end{subfigure}%
  \begin{subfigure}{.45\textwidth}
    \centering
    \includegraphics[width=.7\linewidth]{images/stencil2_actors.png}
    \caption{\texttt{stencil-multi}}
  \end{subfigure}%
  \caption{\texttt{stencil-single}: The actor at (0,0) in the upper-left is created by
    the host. When it is finished computing its output value it creates one or two other
    actors. This pattern propogates across the output domain so all values are computed.
    \texttt{stencil-multi}: The same actor creation pattern, though each actor
    incorporates multiple CUDA threads and is responsible for multiple corresponding output
    entries.}
  \label{fig:stencil12pattern}
\end{figure}

Figure \ref{fig:stencil12perf} shows the relative performance of the two stencil
implementations across different input sizes. \texttt{stencil-single} is limited to 12x12
images due to CUDA's kernel nesting depth of 24. \texttt{stencil-single} is also much slower
than \texttt{stencil-multi} because it does not make use of the parallelism available
in the GPU. Even if \texttt{stencil-single} could launch enough thread-blocks to fill the
GPU, having only one thread per block would limit the performance. Conversely,
\texttt{stencil-multi} is launched with 1024 threads per block in a 32x32 grid, which is the
maximum size allowed by CUDA.

\begin{figure}[!ht]
  \centering
  \begin{subfigure}{.48\textwidth}
    \centering
    \includegraphics[width=1\linewidth]{images/stencil1perf.png}
    \caption{\texttt{stencil-single}}
  \end{subfigure}%
  \begin{subfigure}{.48\textwidth}
    \centering
    \includegraphics[width=1\linewidth]{images/stencil2perf.png}
    \caption{\texttt{stencil-multi}}
  \end{subfigure}%
  \caption{Due to \texttt{stencil-multi} running with thread-blocks that are 1024 times the
  larger than those in \texttt{stencil-single}, it achieves similar performance on images that
  are 1000 times as large. This demonstrates that just because it is possible use CUDA dynamic
  programming to support actor-style languages, some system-level awareness is necessary to
  achieve resonable performance.}
  \label{fig:stencil12perf}
\end{figure}

The results are not surprising - even though CUDA may support an actor-style approach to this
stencil, some microarchitecture-awareness is necessary to extract reasonable performance.

Like \texttt{stencil-single}, the size of inputs for \texttt{stencil-multi} is limited by the
kernel nesting depth. Since each actor creation is a nested kernel launch and each actor covers
a 32$\times$32 grid of the output, the maximum output size is (32*12) $\times$ (32 * 12).

\subsection{Effect of Message Passing on Stencil Performance}

To get some insight into the cost of actor synchronization through global memory we created
a modified version of \texttt{stencil-multi} called \texttt{stencil-multi-sync}. In 
\texttt{stencil-multi-sync} the actor generation pattern of \texttt{stencil-single} and 
\texttt{stencil-multi} shown in Figure \ref{fig:stencil12pattern} is modified so that each
actor tries to produce all of its adjacent children. So that extra actors are not created,
each actor atomically modifies a global variable and only the last parent who attempts
to create the child will succeed. This new pattern is shown in Figure
\ref{fig:stencil4pattern}.

\begin{figure}[!ht]
  \centering
    \includegraphics[width=.4\linewidth]{images/stencil4_actors.png}
    \caption{\texttt{stencil-multi-sync}: Each child can be created by one of two parents.
    When a parent is ready to create the child, it updates a corresponding global variable
    atomically. Only when a parent is the last to update the variable will it create the
    child. This models passing simple messages through global memory.}
    \label{fig:stencil4pattern}
\end{figure}

The global synchronization variable is similar to a mailbox, and instead of arbitrary messages
all that is needed here is an integer counter. When a parent finds that exactly one or two other
parents
have attempted to create the child (depending on the child's location) it will create the
new actor through dynamic parallelism. Like \texttt{stencil-multi}, the maximum output size
is 384$\times$384.

\begin{figure}[!ht]
  \centering
  \begin{subfigure}{.48\textwidth}
    \centering
    \includegraphics[width=1\linewidth]{images/stencil2perf_rescale4.png}
    \caption{\texttt{stencil-multi}}
  \end{subfigure}%
  \begin{subfigure}{.48\textwidth}
    \centering
    \includegraphics[width=1\linewidth]{images/stencil4perf.png}
    \caption{\texttt{stencil-multi-sync}}
  \end{subfigure}%
  \caption{The synchronization adds a substantial overhead in \texttt{stencil-multi-sync}, at
  worst providing a speedup up 0.79 for large image sizes.}
  \label{fig:stencil24perf}
\end{figure}

The expense of sending the "message" in \texttt{stencil-multi-sync} on the same order of
magnitude as the amount of computation done by a thread. Even though only one thread per block
is sending the message all other threads must wait, so the overhead is not amortized by
adding many threads to a single thread-block actor. The only way to amortize the
message-passing work is to have each thread do more non-message work, but CUDA is designed
to spread work across threads to achieve maximum performance. In practice, emulating
message-passing will often incurr a significant penalty if CUDA is the foundation of the actor
language.

\subsection{Another Synchronization Style}  
We created one final stencil implementation, \texttt{stencil-central}, to get around the kernel
nesting limit and test another style of synchronization. For stencil, it is possible to launch
one parent actor that does not do any work and covers the output with independent child
kernel launches in parallel. This pattern is shown in Figure \ref{fig:stencil3pattern}.

\begin{figure}[!ht]
  \centering
    \includegraphics[width=.4\linewidth]{images/stencil3_actors.png}
    \caption{\texttt{stencil-central}: A single parent creates all children, who are at a
    nesting level of 2.}
    \label{fig:stencil3pattern}
\end{figure}

Here, the synchronization between the parent and child actors is implicitly encoded in the
CUDA kernel nesting semantics, since the parent kernel will be live for as long as its children
are. This achieves performance higher than even \texttt{stencil-multi} for images that are large
since it forgoes the explicit synchronization messages in global memory.

\begin{figure}[!ht]
  \centering
  \begin{subfigure}{.48\textwidth}
    \centering
    \includegraphics[width=1\linewidth]{images/stencil2perf_rescale3.png}
    \caption{\texttt{stencil-multi}}
  \end{subfigure}%
  \begin{subfigure}{.48\textwidth}
    \centering
    \includegraphics[width=1\linewidth]{images/stencil3perf.png}
    \caption{\texttt{stencil-central}}
  \end{subfigure}%
  \caption{\texttt{stencil-central} is faster for large input sizes due to implicitly encoding
  synchronization messages into the CUDA nested kernel semantics.}
  \label{fig:stencil23perf}
\end{figure}

Figure \ref{fig:stencil23perf} shows the performance of \texttt{stencil-multi} compared to
\texttt{stencil-central}. For large input sizes, \texttt{stencil-central} is faster.

\subsection{Embarrassingly parallel, no synchronization type programs}
As mentioned in the previous sections, attempting to synchronize actors on a CUDA device
incurs a significant performance cost, thus programs that require synchronization may not lend 
themselves well to an actor model on a CUDA device. For example, the stencil program needed some 
modification to minimize communication between actors in order to perform better. 
However there exists a class of problems where there is no dependency or communication
between parallel tasks, and thus may show a greater benefit from an CUDA actor 
implementation. Mandelbrot is one such type of problem. Each pixel in the Mandelbrot set 
are calculated completely independent from each other. \\

For the purpose of this experiment we developed two versions of the Mandelbrot set problem. 
The first version is computed simply by dividing the image evenly into a grid of blocks of 
thread, mapping each CUDA thread to a pixel in the image as is shown in 
figure \ref{fig:naive_mandel}. This version is very straight forward, taking less than 20 
lines of code to implement in CUDA.

\begin{figure}[!ht]
  \centering
    \includegraphics[width=.4\linewidth]{images/naive_Mandelbrot.png}
    \caption{\texttt{Regular/Non-dynamic Mandelbrot}: The image is computed by subdividing it
    into equally size CUDA blocks where each thread in a block gets assigned a pixel in the image.}
    \label{fig:naive_mandel}
\end{figure}

However, this version suffers from one very significant issue. A great number of threads actually 
perform pointless calculation, computing non-essential or non-interesting section of the image. 
This inefficiency actually leads us into our second implementation of Mandelbrot set. Our second 
approach leverages CUDA dynamic parallelism to solve the same problem following the actor model.
First, similar to our first approach, the image is subdivided evenly into a grid of actors.
Then each
actor determines if its section of the image is interesting enough by sampling the center
and the for corners of its corresponding region and performing some calculations. This is done in 
parallel using multiple threads in an actor. If the region is not interesting it will assigned the 
average value of the sampled point into each pixel and stop. However, If the actor determines
that its region is interesting enough, and too large to compute by itself, it will create 
a grid of actors and assigned to each one of them a sub-section of its region. This effectively 
performs mesh refinement over the image. An example of this idea can bee seen in figure 
\ref{fig:dynamic_mandel} \\


\begin{figure}[!ht]
  \centering
    \includegraphics[width=.4\linewidth]{images/dynamic_Mandelbrot.png}
    \caption{\texttt{Dynamic Mandelbrot}: Each square is an actor responsible for a region of the image.
    Actors that where created by other actors are represented by smaller squares within other squares/actors.}
    \label{fig:dynamic_mandel}
\end{figure}

We measure the execution time of both approaches with multiple size images. The simple non-dynamic CUDA 
version is run by decomposing the image into thread blocks with 32$\times$32 threads, the maximum number of threads per block 
allowed by CUDA. The exact number of blocks that are required to decompose the image is computed at run-time by the program. The Dynamic Mandelbrot first divides the images evenly into 16 actors of 16 threads each. The actors
will follow the algorithm described above and generate other actors if needed. We compare the performance of
both approaches in Figure \ref{fig:dynamic_vs_non}. \\

\begin{figure}[!ht]
  \centering
    \includegraphics[width=.8\linewidth]{images/Dynamic_vs_Non2.png}
    \caption{\texttt{Dynamic vs Non-Dynamic Mandelbrot}: Dynamic Mandelbrot performs much faster than 
    its Non-Dynamic counterpart due to its ability to only compute the section of the image that matters regardless
    of the size of the image, minimizing usless computation.}
    \label{fig:dynamic_vs_non}
\end{figure}

The result of this experiment shows the benefit of CUDA dynamic parallelism following the 
actor model, over the regular CUDA implementation. The ability to dynamically create actors when needed,
and avoid useless computation becomes essential in the performance improvements over the regular implementation.
Even though the non-dynamic version lends itself well to the massivelly parrallel execution model
of GPU hardware, the execution time will increase as the image increase.\\

What we can take from this results is the benefits of the actor model on a CUDA setting. Whenever
we have an application that is "Embarrassingly Parallel," thus no synchronization is involve, it could
potentially benefit from a dynamic CUDA approach.


\section{Related Work}

There is limited related work in this field.

DeLorimier's Ph.D thesis \cite{delorimier2013graph} describes a high-level actor language for
parallel graph algorithms. This language is mapped onto a pre-existing framework that can compile
code onto FPGAs by creating a large number of simple computing elements. He mentions that the
compiler backend can be easily retargeted to GPUs, but that is outside the scope of his thesis.
This work shows that certain types of actor problems can be efficiently mapped onto
highly-parallel architectures in practice.

Charousset, Schmidt, and Hiesgen \cite{charousset2013native} develop a set of C++ extensions for
actor programming that includes the ability to describe actor behavior in OpenCL. This allows 
one actor to run on a GPU. This differs from our goal where all actors run on the GPU with host
interaction only occuring to launch the first kernel.

Fabio Correa from the University of Pretoria \cite{correa2009actors} propose similar work to our report.
He suggest to measure the strengths and weaknesses of current available actor-model implementations against 
the omnipresent shared-state, multi-threaded model of concurrent programming such as GPUs. We are unsure 
on the current state of this work. 


\section{Conclusion}

In this report we try to evaluate the possibility of leveraging CUDA dynamic parallelism for a
future actor implementation in CUDA. We enumerated and showed a mapping between key actor characteristics
and CUDA dynamic parallelism properties that enables a actor-style programing in CUDA. Depending on the
application, the difficulty of applying an actor model could vary but even if a program can be implemented in an actor-syle, some architecture-awareness is necessary to get reasonable performance.
The lack of good and efficient messaging and synchronization clearly limits the benefits of a possible
actor implementation. Currently, for CUDA to be a reasonable actor model, the number of messages passed 
will have to be minimized. Also, taking advantage of CUDA semantics to encode some 
actor semantics for specific types of problems should be done whenever possible. On the other hand 
applications that do not require synchronizations or/and message passing, could beneffit greatly from 
such models as shown by the Mandelbrot example.

\bibliographystyle{plain}
\bibliography{grcdgnz2-pearson-cs524}

\end{document}