#include "support.hpp"
#include "kernel.cu"
#include <iostream>
#include <cstdio>

int main(int argc, char**argv) {

    Timer timer;
    cudaError_t cuda_ret;

    // Initialize host variables ----------------------------------------------

    printf("\nSetting up the problem..."); fflush(stdout);
    startTime(&timer);

    int INPUT_SIZE_X, INPUT_SIZE_Y;
    int THREADS_PER_BLOCK_x, THREADS_PER_BLOCK_y;
    int NUM_LAYERS;
    if(argc == 1) {
      INPUT_SIZE_X = 12;
      INPUT_SIZE_Y = 12;
      THREADS_PER_BLOCK_x = 16;
      THREADS_PER_BLOCK_y = 16; 
      NUM_LAYERS = 1;
    } else if(argc == 6) {
        INPUT_SIZE_X = std::atoi(argv[1]);
        INPUT_SIZE_Y = std::atoi(argv[2]);
    	THREADS_PER_BLOCK_x = std::atoi(argv[3]);
	THREADS_PER_BLOCK_y = std::atoi(argv[4]);
	NUM_LAYERS = std::atoi(argv[5]);
    } else {
        printf("\n    Invalid input parameters!"
           "\n    Usage: ./vecadd # 12 12 4 4 is used"
           "\n    Usage: ./vecadd <input x> <input y> <actor x> <actor y>"
           "\n");
        exit(0);
    }

    const int ACTOR_LAUNCHES_X = (INPUT_SIZE_X-1) / THREADS_PER_BLOCK_x + 1;
    const int ACTOR_LAUNCHES_Y = (INPUT_SIZE_Y-1) / THREADS_PER_BLOCK_y + 1;
    //std::cout << "xlaunches: " << ACTOR_LAUNCHES_X << "\n";
    //std::cout << "ylaunches: " << ACTOR_LAUNCHES_Y << "\n";




    // Input
    float *A_h = new float[INPUT_SIZE_Y * INPUT_SIZE_X];
    for (unsigned int i = 0; i < INPUT_SIZE_Y * INPUT_SIZE_X; ++i) { A_h[i] = 1;} //(rand()%100)/100.00; }

    // Output
    float* B_h = new float[INPUT_SIZE_Y * INPUT_SIZE_X];
    for (unsigned int i = 0; i < INPUT_SIZE_Y * INPUT_SIZE_X; ++i) { B_h[i] = 1;}


    stopTime(&timer); printf("%f s\n", elapsedTime(timer));
    printf("    Size y=%u, x=%u\n", INPUT_SIZE_X, INPUT_SIZE_Y);

    // Allocate device variables ----------------------------------------------

    printf("Allocating device variables..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    float* A_d;
    cuda_ret = cudaMalloc((void**) &A_d, sizeof(float)*INPUT_SIZE_X*INPUT_SIZE_Y);
	if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    float* B_d;
    cuda_ret = cudaMalloc((void**) &B_d, sizeof(float)*INPUT_SIZE_X*INPUT_SIZE_Y);
	if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    float* temp;
    cuda_ret = cudaMalloc((void**) &temp, sizeof(float));
        if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    cudaStream_t* s;
    cuda_ret = cudaMalloc((void**) &s, sizeof(cudaStream_t)*ACTOR_LAUNCHES_X*ACTOR_LAUNCHES_Y);
        if(cuda_ret != cudaSuccess) FATAL("Unable to allocate device memory");

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Copy host variables to device ------------------------------------------

    printf("Copying data from host to device..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    cuda_ret = cudaMemcpy(A_d, A_h, sizeof(float)*INPUT_SIZE_X*INPUT_SIZE_Y,
                          cudaMemcpyHostToDevice);
    if(cuda_ret != cudaSuccess) FATAL("Unable to copy memory to device");


    cuda_ret = cudaMemcpy(B_d, B_h, sizeof(float)*INPUT_SIZE_X*INPUT_SIZE_Y,
                          cudaMemcpyHostToDevice);
    if(cuda_ret != cudaSuccess) FATAL("Unable to copy memory to device");


    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Launch kernel ----------------------------------------------------------

    printf("Launching kernel..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    dim3 gridDim(1, 1, 1), blockDim(ACTOR_LAUNCHES_X, ACTOR_LAUNCHES_Y, 1);
    stencilKernel3_2<<< gridDim, blockDim >>> (A_d, B_d, temp, s, 
                                             INPUT_SIZE_X, INPUT_SIZE_Y,
                                             0, 0, NUM_LAYERS, THREADS_PER_BLOCK_x, THREADS_PER_BLOCK_y);
   
     cuda_ret = cudaDeviceSynchronize();
	if(cuda_ret != cudaSuccess) FATAL("Unable to launch kernel");
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Copy device variables from host ----------------------------------------

    printf("Copying data from device to host..."); fflush(stdout);
    startTime(&timer);

    //INSERT CODE HERE
    cuda_ret = cudaMemcpy(B_h, B_d, sizeof(float)*INPUT_SIZE_X*INPUT_SIZE_Y, cudaMemcpyDeviceToHost);
	if(cuda_ret != cudaSuccess) FATAL("Unable to copy memory from device");

    cudaDeviceSynchronize();
    stopTime(&timer); printf("%f s\n", elapsedTime(timer));

    // Verify correctness -----------------------------------------------------

    printf("Verifying results..."); fflush(stdout);

    verify(A_h, B_h, INPUT_SIZE_X, INPUT_SIZE_Y);

    // Free memory ------------------------------------------------------------
    free(A_h);
    free(B_h);

    //INSERT CODE HERE
    cudaFree(A_d);
    cudaFree(B_d);

    return 0;
}
