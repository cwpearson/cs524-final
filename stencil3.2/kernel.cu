__global__ void slaveKernel(float* inputs, float* outputs,
                            const int INPUT_SIZE_X,
                            const int INPUT_SIZE_Y,
                            int x, int y){


  if((x + threadIdx.x) < INPUT_SIZE_X - 1 && (y + threadIdx.y) < INPUT_SIZE_Y - 1) {

    int i = (y + threadIdx.y) * INPUT_SIZE_X + (x + threadIdx.x);
    int n_right = (y + threadIdx.y)*INPUT_SIZE_X + ((x + threadIdx.x)+1);
    int n_down = ((y + threadIdx.y)+1)*INPUT_SIZE_X + (x + threadIdx.x);

    outputs[i] = inputs[i] + inputs[n_right] + inputs[n_down];
  }

}


__global__ void stencilKernel3_2(float* inputs, float* outputs, float* temp, cudaStream_t* s,
                               const int INPUT_SIZE_X, 
                               const int INPUT_SIZE_Y,
                               int x, int y, int layers,
			       const int THREAD_SIZE_X, 
			       const int THREAD_SIZE_Y) {
  
  //int NUM_ACTORS_X = (INPUT_SIZE_X - 1) / THREAD_SIZE_X + 1; 
  //int NUM_ACTORS_Y = (INPUT_SIZE_Y - 1) / THREAD_SIZE_Y + 1; 

  dim3 gridDim(1,1,1);
  dim3 blockDim(THREAD_SIZE_X, THREAD_SIZE_Y, 1);

  //float* temp;

  for (int i = layers; i > 0; i--)
  {
	cudaStreamCreateWithFlags(&s[threadIdx.y*blockDim.x+threadIdx.x], cudaStreamNonBlocking);
	slaveKernel<<< gridDim, blockDim, 0 >>> (inputs, outputs, INPUT_SIZE_X, INPUT_SIZE_Y, threadIdx.x*THREAD_SIZE_X, threadIdx.y*THREAD_SIZE_Y);
	
	//__syncthreads();
	cudaDeviceSynchronize();
	
	//if(threadIdx.x == 0 && threadIdx.y == 0)
	//{
		temp = inputs;
                inputs = outputs;
                outputs = temp;		
	//}

	__syncthreads();

  }



 /* if(threadIdx.x == 0 && threadIdx.y == 0) 
  {
	dim3 gridDim(1,1,1);
	dim3 blockDim(THREAD_SIZE_X, THREAD_SIZE_Y, 1);

	for (int l = layers; l > 0; l--)
	{

		for(int k = 0; k < NUM_ACTORS_Y; k++)
		{
			for(int j = 0; j < NUM_ACTORS_X; j++)
			{
				
				cudaStreamCreateWithFlags(&s[k*NUM_ACTORS_X+j], cudaStreamNonBlocking);	
				slaveKernel<<< gridDim, blockDim, 0, s[k*NUM_ACTORS_X+j]>>> (inputs, outputs, INPUT_SIZE_X, INPUT_SIZE_Y, j*THREAD_SIZE_X, k*THREAD_SIZE_Y);
			}		
		}

		cudaDeviceSynchronize();

		float* temp = inputs;
		inputs = outputs;
		outputs = temp;
	}
  }*/

}


