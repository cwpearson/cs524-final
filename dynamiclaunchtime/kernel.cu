
__global__ void dynamicLaunch(int depth, int maxDepth) {
  if (depth + 1 <= maxDepth)
    dynamicLaunch<<< gridDim, blockDim>>> (depth+1, maxDepth);
}

